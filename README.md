**Jenkins Installation and setup in ubuntu 18.04**

> Installation Steps 

- [ sudo apt update ]
- [ sudo apt install openjdk-8-jdk]
- [ wget –q –O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add – ]
- [ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 5FCBF54A ]
- [ sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list' ]
- [ sudo apt update ]
- [ sudo apt install jenkins ]
- [ sudo systemctl start jenkins ]
- [ sudo systemctl enable jenkins ]

> Setup Steps

- Create the admin user account 
- Create a piepline job
- Go to configure tab
- Go to Build Triggers section  
    -  Enable the poll SCM 
- Go to pipeline section
    -  Add the pipeline script provided here in the **_master_** branch
- Click save 

**Helm server Installation and setup**

> Installation steps

- Here I have used the docker image for helm server

    - kiwigrid/gcloud-kubectl-helm

- Installing by executing the below docker command 
  
    - docker run -id -v /_<HOST_MACHINE_VOLUME_PATH>_:/_<CONTAINER_VOLUME_PATH>_ --name _<HELM_SERVER_NAME>_ kiwigrid/gcloud-kubectl-helm bash
    
    - [docker run -id -v /usr/local/poc/:/data/ --name helm-gcloud kiwigrid/gcloud-kubectl-helm bash]

- Connect the helm container by executing the below command
    
    - docker exec -it helm-gcloud /bin/bash

- Go to the Host machine volume location and clone the helm charts from **_helm_** branch.

- Go to the Host machine volume location and get the key file for the serviceaccount in GCP to access the kubernetes cluster & paste it here with the name of _<SERVICE_ACCOUNT_KEY.json>_ or use GCP account details directly.

- Once done, connect the container and execute the below command with required change in values
    - [ gcloud auth activate-service-account --key-file=/data/_<SERVICE_ACCOUNT_KEY.json>_ ]
    - [ gcloud container clusters get-credentials _CLUSTER NAME_ --project=_<PROJECT_ID>_ --region=_CLUSTER REGION_ ]

**Docker Image preparation**

Before going into the image preparation check the compatability & dependencies

- Samplepjt application specification

| Item | version |
| ------ | ------ |
| centos | 7 |
| php |5.4.16 |
| mediawiki | 1.26.3|
| mysql | 5.7 |

**Mysql Details**

- Database details has to be updated in the _LocalSettings.php_ file to access the database before build docker image.

- Dockerfile here to build the mediawiki application

**Parameters to check and change in jenkins pipeline script**

***Variables details****

|Variable|Values|Action|Explanation|
|------|-------|---------|-------|
|env.MYIMAGE_VERSION | $BUILD_NUMBER-stable| Not Required |Docker Image version|
|env.ENV_VALUES | test_values.yaml   |Not Required|Values file for environment specific|
|env.PROJECT_NAME | sound-splicer-291211|Required| Kubernetes Project name|
|env.CLUSTER_NAME | samplepjt|Required| Kubernetes Cluster ID|
|env.ACCOUNT_NAME | dhevasenapathi.s@gmail.com|Required|GCP Account/Service Account json Key|
|env.CLUSTER_REGION | us-central1-c|Required|Kubernetes Cluster Region|
|env.HELM_CONTAINER_NAME | helm-gcloud|Required|Helm Container name|
|env.HELM_PATH | /usr/local/poc/helm/|Required|Helm path in the host machine|
|env.CONTAINER_HELM_PATH |/data/helm/"|Required|Helm path inside container|
|env.HELM_BRANCH | helm|Required|Helm repo branch|

**Parameters to check and change in Helm Charts**

***Variables details****

_Have to alter only the test_values.yaml file in **env** folder in helm charts_

- As of now not required to change the values file, in future any parameters added have to alter this file.

**Deployment Scripts**

- Execute the deployment_gke.sh script to do the deployment with six parameters in the kubernetes cluster.

- sh deployment_gke.sh _$JOB_NAME_ _$BUILD_NUMBER_ _$MYIMAGE_VERSION_ _$ENV_VALUES_ _$HELM_CONTAINER_NAME_ _$HELM_PATH_ 
    - Explanation already stated in the above variables table

Testing purpose
