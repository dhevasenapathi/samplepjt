FROM centos:7

# Install Apache
RUN yum -y update
RUN yum -y install httpd httpd-tools

# Install EPEL Repo
RUN yum -y install epel-release

# Install PHP
RUN yum install httpd php php-mysqlnd php-gd php-xml mariadb-server mariadb php-mbstring php-json wget -y

# Update Apache Configuration
WORKDIR /var/www
RUN sed -E -i -e '/<Directory "\/var\/www\/html">/,/<\/Directory>/s/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
RUN sed -E -i -e 's/DirectoryIndex (.*)$/DirectoryIndex index.php \1/g' /etc/httpd/conf/httpd.conf
RUN sed -E -i -e 's/DocumentRoot "\/var\/www\/html"/DocumentRoot "\/var\/www\/mediawiki"/g' /etc/httpd/conf/httpd.conf
RUN sed -E -i -e 's/<Directory "\/var\/www\/html">/<Directory "\/var\/www\/mediawiki">/g' /etc/httpd/conf/httpd.conf
RUN wget https://releases.wikimedia.org/mediawiki/1.26/mediawiki-1.26.3.tar.gz
RUN wget https://releases.wikimedia.org/mediawiki/1.26/mediawiki-1.26.3.tar.gz.sig
RUN tar -zxf /var/www/mediawiki-1.26.3.tar.gz
RUN ln -s mediawiki-1.26.3/ mediawiki
#LocalSetting file has to be changed based on the DB details
COPY LocalSettings.php /var/www/mediawiki
RUN chown -R apache:apache /var/www/mediawiki

#SElinux 
RUN yum install policycoreutils policycoreutils-python selinux-policy selinux-policy-targeted libselinux-utils setroubleshoot-server setools setools-console mcstrans -y
RUN sed -i 's/SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config

EXPOSE 80

# Start Apache
CMD ["/usr/sbin/httpd","-D","FOREGROUND"]